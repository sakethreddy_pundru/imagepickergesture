//
//  ViewController.m
//  ImagePickerAction
//
//  Created by admin on 16/10/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import "ViewController.h"
@import MobileCoreServices;

@interface ViewController ()

@end

@implementation ViewController{
    UIImage *image;
    UIImagePickerController *imagePicker;
    UIImageView *imageView;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    imageView = [[UIImageView alloc]init];
    imageView.frame = CGRectMake(20, 50, 200, 200);
    [self.view addSubview:imageView];
    imageView.backgroundColor = [UIColor redColor];
    imageView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]init];
    gesture.numberOfTapsRequired = 1;
    [gesture addTarget:self action:@selector(tap)];
    [imageView addGestureRecognizer:gesture];
    
}


-(void)tap{

    
    imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    imagePicker.mediaTypes = [[NSArray alloc]initWithObjects:(NSString *)kUTTypeImage,(NSString *)kUTTypeVideo, nil];
    [self presentViewController:imagePicker animated:YES completion:nil];
    }

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    if ([[info valueForKey:UIImagePickerControllerMediaType]isEqualToString:@"public.image"]) {
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
  
    imageView = [[UIImageView alloc]initWithImage:image];

    imageView.frame = CGRectMake(20, 50, 200, 200);
    [self.view addSubview:imageView];
    
    [imagePicker dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [imagePicker dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    }

@end
